use std::{env, process, thread, time};
use std::io::{prelude::*, BufReader};
use std::net::TcpStream;

struct UserArgs {
    host: String,
    port: u16,
    delay: u64,
}

impl UserArgs {
    fn new(args: &[String]) -> Result<UserArgs, &'static str> {
        let usage_message = "Usage: ./rust-cli <URL> <PORT> <DELAY-milliseconds>";

        if args.len() < 4 {
            return Err(usage_message);
        }

        let host = args[1].clone();
        let port = args[2].clone();
        let delay = args[3].clone();

        match port.parse::<u16>() {
            Ok(port) => match delay.parse::<u64>() {
                Ok(delay) => Ok(UserArgs { host, port, delay }),
                Err(_) => Err("DELAY should be positive integer number")
            },
            Err(_) => Err("PORT should be positive integer number")
        }
    }
}

fn format_get_request(host: &String) -> String {
    format!("GET / HTTP/1.0\r\nHost: www.{}\r\n\r\n", host)
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();

    let user_args = UserArgs::new(&args).unwrap_or_else(|err| {
        println!("{}", err);
        process::exit(1);
    });

    let host = &user_args.host;
    let delay = user_args.delay;
    let port = user_args.port;

    let request = format_get_request(host);
    
    loop {
        let mut stream = TcpStream::connect(format!("{}:{}", host, port))?;
        stream.write_all(request.as_bytes())?;

        match BufReader::new(stream).lines().nth(0) {
            Some(Ok(line)) => println!("{}", line),
            Some(Err(err)) => eprintln!("{}", err),
            None => ()
        }

        thread::sleep(time::Duration::from_millis(delay));
    }
}
